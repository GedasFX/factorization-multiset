package projMilasiusTests;

import projMilasius.Util.TreeMultiSet;

import java.util.Arrays;

public class TestsTreeMultiSet {
    private TreeMultiSet<String> treeSet;
    private final String[] strings = {
        "String1", "String2", "String2", "String2", "String1", "", "String5", "String5"
    };

    public static void main(String[] args) {
        new TestsTreeMultiSet();
    }

    public TestsTreeMultiSet() {
        this.treeSet = new TreeMultiSet<>();
        System.out.println(unitTests());
    }

    private int unitTests() {
        int failedCount = 0;

        failedCount += size(0) ? 0 : 1;
        failedCount += isEmpty(true) ? 0 : 1;
        failedCount += addAll() ? 0 : 1;
        failedCount += addNull() ? 0 : 1;
        failedCount += size(strings.length) ? 0 : 1;
        failedCount += isEmpty(false) ? 0 : 1;
        failedCount += remove("String1") ? 0 : 1;
        failedCount += contains("String1") ? 0 : 1;
        failedCount += remove("String1") ? 0 : 1;
        failedCount += size(strings.length - 2) ? 0 : 1;
        failedCount += contains("String1") ? 1 : 0;
        failedCount += getCount("String2", 3) ? 0 : 1;
        failedCount += subtract("String2", 50) ? 0 : 1;
        failedCount += getCount("String2", 0) ? 0 : 1;
        failedCount += clear() ? 0 : 1;
        failedCount += addAll() ? 0 : 1;
        failedCount += retainAll() ? 0 : 1;
        failedCount += contains("String5") ? 1 : 0;
        failedCount += add("String") ? 0 : 1;
        failedCount += containsAll() ? 0 : 1;
        failedCount += setCount("Meh", 20) ? 0 : 1;
        failedCount += size(26) ? 0 : 1;
        failedCount += subtract("Meh", 15) ? 0 : 1;
        failedCount += size(11) ? 0 : 1;
        failedCount += toArray(new String[0]) ? 0 : 1;
        failedCount += toArray(new Integer[0]) ? 1 : 0;
        failedCount += toArray(new Object[0]) ? 0 : 1;
        failedCount += clear() ? 0 : 1;
        failedCount += setCount("Meh", 8) ? 0 : 1;
        failedCount += editCount("Meh", 5, "Meh1") ? 0 : 1;
        failedCount += editCount("Meh", 5, "Meh2") ? 0 : 1;


        return failedCount;
    }

    private boolean editCount(String meh, int i, String meh1) {
        int count1 = treeSet.getCount(meh);
        int count2 = treeSet.getCount(meh1);
        treeSet.edit(meh, i, meh1);
        int count3 = treeSet.getCount(meh);
        int count4 = treeSet.getCount(meh1);
        return count1 + count2 == count3 + count4;
    }

    private <E> boolean toArray(E[] type) {
        try {
            E[] e = treeSet.toArray(type);
            if (type instanceof Integer[]) {
                ((Integer[])e)[1].doubleValue();
            }
            int a = e.length;
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private boolean setCount(String test, int i) {
        treeSet.setCount(test, i);
        return getCount(test, i);
    }

    private boolean add(String string) {
        return treeSet.add(string);
    }

    private boolean containsAll() {
        return treeSet.containsAll(Arrays.asList("String1", "String2"));
    }

    private boolean retainAll() {
        return treeSet.retainAll(Arrays.asList("String1", "String2"));
    }

    private boolean clear() {
        treeSet.clear();
        return size(0);
    }

    private boolean subtract(String el, int count) {
        int initCount = treeSet.getCount(el);
        treeSet.subtract(el, count);
        return treeSet.getCount(el) == 0 || treeSet.getCount(el) + count == initCount;
    }

    private boolean getCount(String el, int expectedCount) {
        return treeSet.getCount(el) == expectedCount;
    }

    private boolean contains(String string) {
        return treeSet.contains(string);
    }

    private boolean isEmpty(boolean reallyEmpty) {
        return treeSet.isEmpty() == reallyEmpty;
    }

    private boolean remove(String remove) {
        return treeSet.remove(remove);
    }

    private boolean size(int expectedSize) {
        return treeSet.size() == expectedSize;
    }

    private boolean addNull() {
        try {
            treeSet.add(null);
            return false;
        } catch (IllegalArgumentException ex) {
            return true;
        }
    }

    private boolean addAll() {
        return treeSet.addAll(Arrays.asList(strings));
    }

}
