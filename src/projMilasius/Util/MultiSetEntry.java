package projMilasius.Util;

/**
 * Immutable MultiSet entry class.
 * Used in outing result.
 * @param <E> Type
 */
public class MultiSetEntry<E> implements Comparable<MultiSetEntry> {
    private E key;
    private int count;

    public MultiSetEntry(E key, int count) {
        this.key = key;
        this.count = count;
    }

    /**
     * Gets the amount of an element in the MultiSet.
     * Always above 0.
     * @return Amount of a single element.
     */
    public int getCount() {
        return count;
    }

    /**
     * Gets the element of the entry.
     * @return Element.
     */
    public E getElement() {
        return key;
    }

    @Override
    public int compareTo(MultiSetEntry o) {
        if (key instanceof Comparable)
            return ((Comparable) key).compareTo(o.getElement());
        return Integer.compare(key.hashCode(), o.getElement().hashCode());
    }
}
