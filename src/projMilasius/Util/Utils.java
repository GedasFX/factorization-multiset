package projMilasius.Util;

import java.io.*;
import java.math.BigInteger;
import java.util.Random;

public class Utils {
    /**
     * Generates and writes numbers.
     * @param count Amount of numbers to be generated
     * @param length Bit length of a single number.
     * @param filePath Path to file
     * @throws IOException
     */
    public static void generateNumbers(int count, int length, String filePath) {
        try {
            FileWriter writer = new FileWriter(filePath);
            Random random = new Random(73_70_70_45_54_47_50L); // ASCII for IFF-6/2
            for (int i = 0; i < count; i++) {
                BigInteger bigInt = new BigInteger(length, random);
                writer.write(bigInt.toString());
                writer.write(System.lineSeparator());
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
