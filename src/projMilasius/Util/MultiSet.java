package projMilasius.Util;

import java.util.Collection;
import java.util.Set;

/**
 * MultiSet.
 * Set, where multiple elements can exist.
 * Stores elements and occurrences of said element.
 * @param <E> Type
 */
public interface MultiSet<E> extends Collection<E> {
    /**
     * Returns the amount of a given element in the list.
     * Returns 0 if element is non-existent.
     * @param element Element to get the count of.
     * @return Amount of occurrences in the set.
     */
    int getCount(E element);

    /**
     * Sets the amount of a given item in the list.
     * If element is non-existent, it creates it.
     * @param element Element to add.
     * @param count Amount to add.
     */
    void setCount(E element, int count);

    /**
     * Adds an amount of a given element.
     * If element doesn't exist, the element is added to the list.
     * @param element Element to add to.
     * @param count Amount to add.
     */
    void add(E element, int count);

    /**
     * Adds the contents of another multiset to the current one.
     * @param set
     */
    void addMultiSet(MultiSet<E> set);

    /**
     * Subtracts an amount of a given element.
     * If subtract amount is greater or equal to the amount in the list, the element is removed.
     * @param element Element to subtract from.
     * @param count Amount to subtract.
     */
    void subtract(E element, int count);

    /**
     * Edits a set amount of elements of the MultiSet to a different element.
     * If the amount is greater than the amount in the set, uses the number in the set.
     * @param element Element to edit.
     * @param amount Amount to edit.
     * @param toElement The element to edit to.
     */
    void edit(E element, int amount, E toElement);

    /**
     * Edits one element of the MultiSet to another element.
     * @param element Element to edit.
     * @param toElement The element to edit to.
     */
    void editOne(E element, E toElement);

    /**
     * Edits all elements of the MultiSet to another element.
     * @param element Element to edit.
     * @param toElement The element to edit to.
     */
    void editAll(E element, E toElement);

    /**
     * Converts the MultiSet to a regular Set.
     * @return A set of all unique elements.
     */
    Set<E> elementSet();

    /**
     * Converts the MultiSet to a Set of the entries.
     * Entires consist of the element and the amount of occurrences.
     * @return A set of all entries.
     */
    Set<MultiSetEntry<E>> entrySet();
}