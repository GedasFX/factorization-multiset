package projMilasius.Util;

import projMilasius.Util.Exception.ElementNonExistentException;

import java.math.BigInteger;
import java.util.*;
import java.util.Map.Entry;

/**
 * Polymorphic multiset based on a TreeMap
 * @param <E> Type
 */
public class TreeMultiSet<E> implements MultiSet<E> {
    private transient TreeMap<E, Counter> frequencyMap;
    private int size;

    public TreeMultiSet() {
        this.frequencyMap = new TreeMap<>();
        this.size = 0;
    }

    /**
     * Copies a preexisting multiset to a new Tree MultiSet.
     * @param set Preexisting set.
     */
    public TreeMultiSet(MultiSet<E> set) {
        this();
        if (set == null) {
            return;
        }
        addMultiSet(set);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) throws IllegalArgumentException {
        if (o == null)
            throw new IllegalArgumentException("Object o is null");
        return frequencyMap.containsKey(o);
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            Map.Entry<E, Counter> current = frequencyMap.firstEntry();
            int count = 0;

            @Override
            public boolean hasNext() {
                if (count < current.getValue().getCount())
                    return true;
                Entry<E, Counter> entry = frequencyMap.higherEntry(current.getKey());
                if (entry != null)
                    if (entry.getValue().getCount() > 0)
                        return true;
                return false;
            }

            @Override
            public E next() {
                if (count < current.getValue().getCount()) {
                    count++;
                    return current.getKey();
                }
                current = frequencyMap.higherEntry(current.getKey());
                count = 1;
                return current.getKey();
            }
        };
    }

    @Override
    public Object[] toArray() {
        Object[] array = new Object[size];
        int j = 0;
        for (Entry<E, Counter> e : frequencyMap.entrySet()) {
            for (int i = 0; i < e.getValue().getCount(); i++) {
                array[j++] = e;
            }
        }
        return array;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        T[] array = (T[])new Object[size];
        int j = 0;
        for (Entry<E, Counter> e : frequencyMap.entrySet()) {
            for (int i = 0; i < e.getValue().getCount(); i++) {
                array[j++] = (T)e.getKey();
            }
        }
        return array;
    }

    @Override
    public boolean add(E e) throws IllegalArgumentException {
        if (e == null)
            throw new IllegalArgumentException("boolean add(E e); e is null");
        Counter counter = frequencyMap.get(e);
        if (counter == null) {
            frequencyMap.put(e, new Counter());
            size++;
            return true;
        }
        counter.increment();
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) throws IllegalArgumentException {
        if (o == null)
            throw new IllegalArgumentException("Object o is null");
        Counter counter = frequencyMap.get(o);
        if (counter == null) {
            return false;
        }
        counter.decrement();
        size--;
        if (counter.getCount() < 1) {
            frequencyMap.remove(o);
        }
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) throws IllegalArgumentException {
        if (c == null)
            throw new IllegalArgumentException("Collection<?> c is null");
        for (Object o : c) {
            if (!frequencyMap.containsKey(o))
                return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) throws IllegalArgumentException {
        if (c == null)
            throw new IllegalArgumentException("Collection<? extends E> c is null");
        boolean success = true;
        for (E e : c) {
            success = success & add(e);
        }
        return success;
    }

    @Override
    public boolean removeAll(Collection<?> c) throws IllegalArgumentException {
        if (c == null)
            throw new IllegalArgumentException("Collection<?> c is null");
        boolean success = true;
        for (Object e : c) {
            success = success & remove(e);
        }
        return success;
    }

    @Override
    public boolean retainAll(Collection<?> c) throws IllegalArgumentException {
        if (c == null)
            throw new IllegalArgumentException("Collection<?> c is null");

        for (Entry<E, Counter> e : frequencyMap.entrySet()) {
            if (!c.contains(e.getKey())) {
                size -= frequencyMap.remove(e.getKey()).getCount();
                return retainAll(c);
            }
        }
        return true;
    }

    @Override
    public void clear() {
        frequencyMap.clear();
        size = 0;
    }

    @Override
    public int getCount(E element) throws IllegalArgumentException {
        if (element == null)
            throw new IllegalArgumentException("int getCount(E element); element is null");
        Counter el = frequencyMap.get(element);
        return el == null ? 0 : el.getCount();
    }

    @Override
    public void add(E element, int occurrences) throws IllegalArgumentException {
        if (element == null)
            throw new IllegalArgumentException("Element e is null");
        if (occurrences < 0)
            subtract(element, occurrences);
        if (occurrences == 0)
            return;
        Counter counter = frequencyMap.get(element);
        if (counter == null) {
            add(element);
            counter = frequencyMap.get(element);
        }
        counter.add(occurrences - 1);
        size += occurrences - 1;
    }

    @Override
    public void addMultiSet(MultiSet<E> set) {
        for (MultiSetEntry<E> e : set.entrySet()) {
            add(e.getElement(), e.getCount());
        }
    }

    @Override
    public void subtract(E element, int count) throws IllegalArgumentException {
        if (element == null)
            throw new IllegalArgumentException("Element e is null");
        if (count <= 0)
            add(element, count);
        Counter counter = frequencyMap.get(element);
        if (counter == null)
            return;
        if (count >= counter.getCount()) {
            frequencyMap.remove(element);
            size -= counter.getCount();
        }
        else {
            counter.subtract(count);
            size -= count;
        }
    }

    @Override
    public void edit(E element, int amount, E toElement) throws ElementNonExistentException, IllegalArgumentException {
        if (element == null || toElement == null)
            throw new IllegalArgumentException("E element or E toElement is null");
        if (amount < 0)
            throw new IllegalArgumentException("Amount must be greater than 0");
        Counter counter = frequencyMap.get(element);
        if (counter == null)
            throw new ElementNonExistentException("Element doesn't exist in the collection");
        int count = counter.getCount();
        subtract(element, amount);
        add(toElement, amount > count ? count : amount);
    }

    @Override
    public void editOne(E element, E toElement) {
        edit(element, 1, toElement);
    }

    @Override
    public void editAll(E element, E toElement) {
        edit(element, Integer.MAX_VALUE, toElement);
    }

    @Override
    public void setCount(E element, int count) throws IllegalArgumentException {
        if (element == null)
            throw new IllegalArgumentException("Element e is null");
        Counter counter = frequencyMap.get(element);
        if (counter == null) {
            if (count > 0) {
                add(element, count);
                return;
            }
            return;
        }
        if (count < 1) {
            frequencyMap.remove(element);
            size -= counter.getCount();
        } else {
            size += count - counter.getCount();
            counter.setCount(count);
        }
    }

    @Override
    public Set<E> elementSet() {
        return frequencyMap.keySet();
    }

    @Override
    public Set<MultiSetEntry<E>> entrySet() {
        TreeSet<MultiSetEntry<E>> set = new TreeSet<>();
        for (Entry<E, Counter> e : frequencyMap.entrySet()) {
            set.add(new MultiSetEntry<>(e.getKey(), e.getValue().getCount()));
        }
        return set;
    }

    /**
     * Counter class.
     * A workaround non-immutable class
     */
    private class Counter implements Comparable<Counter> {
        private int count;

        private Counter(int initialCount) {
            this.count = initialCount;
        }

        private Counter() {
            this.count = 1;
        }

        private void add(int count) {
            this.count+= count;
        }

        private void subtract(int count) {
            this.count-= count;
        }

        private void increment() {
            this.count++;
        }

        private void decrement() {
            this.count--;
        }

        private int getCount() {
            return count;
        }

        private void setCount(int count) {
            this.count = count;
        }

        @Override
        public int compareTo(Counter o) {
            return Integer.compare(count, o.count);
        }

        @Override
        public String toString() {
            return String.valueOf(count);
        }
    }
}
