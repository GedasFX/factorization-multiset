package projMilasius.Util.Exception;

/**
 * Exception to be thrown, when a given element is not in a collection.
 */
public class ElementNonExistentException extends RuntimeException {
    public ElementNonExistentException(String s) {
        super(s);
    }

    public ElementNonExistentException(String message, Throwable cause) {
        super(message, cause);
    }

    public ElementNonExistentException(Throwable cause) {
        super(cause);
    }

    public ElementNonExistentException() {
    }
}
