package projMilasius.Factorization;

import projMilasius.Factorization.Algorithm.PollardRhoAlgorithm;
import projMilasius.Util.MultiSet;
import projMilasius.Util.TreeMultiSet;

import java.math.BigInteger;

/**
 * Factor class for BigInteger.
 * Uses lazy evaluation, and only evaluates factors once.
 * TODO: Quicker and more consistent results for numbers over 10^40.
 */
public class FactorisableBigInteger {
    private BigInteger bigInteger;
    private MultiSet<BigInteger> factors;

    public FactorisableBigInteger(String bigInteger) {
        this.bigInteger = new BigInteger(bigInteger);
    }

    public FactorisableBigInteger(long bigInteger) {
        this.bigInteger = BigInteger.valueOf(bigInteger);
    }

    public FactorisableBigInteger(byte[] bigInteger) {
        this.bigInteger = new BigInteger(bigInteger);
    }

    /**
     * Gets the factors of the element.
     * Uses lazy single-time evaluation.
     * @return MultiSet of all the factors.
     */
    public MultiSet<BigInteger> getFactors() {
        if (factors == null) {
            BigInteger oNumber = bigInteger;
            factorize();
            bigInteger = oNumber;
        }

        return factors;
    }

    /**
     * Gets the original number.
     * @return Original number, intended for factorization.
     */
    public BigInteger getBigInteger() {
        return bigInteger;
    }

    /**
     * Initializes the MultiSet and populates it with the factors of the number.
     * TODO: Middle to high end of the spectrum.
     */
    private void factorize() {
        factors = new TreeMultiSet<>();
        trivialprimes();
        if (bigInteger.shiftRight(24).bitCount() == 0)
            bruteForce();
        else //if (bigInteger.shiftRight(64).bitCount() == 0)
            pollardRho();
        //else if (bigInteger.shiftRight(256).bitCount() == 0)
        //    lenstraEllipticCurve();
        //else if (bigInteger.shiftRight(512).bitCount() == 0)
        //     quadraticSieve();
        //else
        //     generalNumberFieldSieve();
    }

    private void javaNativeSieve() {
        while (bigInteger.bitCount() != 0) {
            BigInteger probPrime = bigInteger.nextProbablePrime();
            factors.add(bigInteger);
            bigInteger = bigInteger.divide(probPrime);
        }
    }

    /**
     * Gets all easy factors - 2.
     */
    private void trivialprimes() {
        while (!bigInteger.testBit(0)) {
            factors.add(BigInteger.valueOf(2));
            bigInteger = bigInteger.shiftRight(1);
        }
    }

    /**
     * Uses General number field sieve algorithm.
     * Used for bit count above 512.
     * https://en.wikipedia.org/wiki/General_number_field_sieve
     */
    private void generalNumberFieldSieve() {

    }

    /**
     * Uses Quadratic sieve algorithm.
     * Used for bit count from 256 to 512.
     * https://en.wikipedia.org/wiki/Quadratic_sieve
     */
    private void quadraticSieve() {

    }

    /**
     * Uses Lenstra elliptic-curve factorization algorithm.
     * Used for bit count from 64 to 256.
     * https://en.wikipedia.org/wiki/Lenstra_elliptic-curve_factorization
     */
    private void lenstraEllipticCurve() {

    }

    /**
     * Uses Pollard's Rho algorithm.
     * Used for bit count 24 to 64.
     * https://en.wikipedia.org/wiki/Pollard%27s_rho_algorithm
     */
    private void pollardRho() {
        factors.addAll(new PollardRhoAlgorithm(bigInteger).getFactors());
    }

    /**
     * Uses a brute force algorithm.
     * Used for bit count up to 24.
     */
    private void bruteForce() {
        int n = bigInteger.intValue();
        int nSq = (int) Math.sqrt(n);

        for (int i = 3; i <= nSq; i += 2) {
            while (n % i == 0) {
                n /= i;
                factors.add(BigInteger.valueOf(i));
            }
        }
        if (n > 2)
            factors.add(BigInteger.valueOf(n));
    }
}
