package projMilasius.Factorization.Algorithm;

import projMilasius.Util.MultiSet;
import projMilasius.Util.TreeMultiSet;

import java.math.BigInteger;
import java.util.Objects;

/**
 * Pollard's Rho algorithm.
 * Used for bit count 24 to 64.
 * https://en.wikipedia.org/wiki/Pollard%27s_rho_algorithm
 */
public class PollardRhoAlgorithm {
    private static final int DEFAULT_ACCURACY = 1_000_000;

    private MultiSet<BigInteger> factors;
    private BigInteger number;
    private int accuracy;

    private final BigInteger bigZERO = BigInteger.valueOf(0);
    private final BigInteger bigONE  = BigInteger.valueOf(1);
    private final BigInteger bigTWO  = BigInteger.valueOf(2);


    /**
     * Creates a number ready for factorization.
     * @param number Number to factorize.
     */
    public PollardRhoAlgorithm(BigInteger number) {
        this(number, DEFAULT_ACCURACY);
    }

    /**
     * Creates a number ready for factorization.
     * Accuracy is the amount of cycles to run the algortighm. Higher number means higher accuracy.
     * @param number
     * @param accuracy
     */
    public PollardRhoAlgorithm(BigInteger number, int accuracy) {
        this.number = number;
        this.accuracy = accuracy;
    }

    // g(x) = (x^2 + 1) mod n
    private BigInteger modPower(BigInteger x, BigInteger n) {
        return x.multiply(x).add(bigONE).remainder(n);
    }

    private BigInteger factorOne(BigInteger n) {
        if (n.equals(bigONE)) return n;

        BigInteger x = (bigTWO).remainder(n);
        BigInteger y = x.add(bigZERO);

        BigInteger d = BigInteger.valueOf(1);

        int cycleCount = accuracy;
        while (d.equals(bigONE)) {
            if (cycleCount-- == 0)
                break;
            x = modPower(x, n);              // x = g(x)
            y = modPower(modPower(y, n), n); // y = g(g(y))
            d = x.subtract(y).abs().gcd(n);  // d = GCD(|x-y|, n)

            // Case, where it is failed to find a divisor.
            // Assume the number is a prime
            if (Objects.equals(d, n))
                return n;
        }

        return d.equals(bigONE) ? n : d;
    }

    /**
     * Calcualates factors of a BigInteger and stores them internally.
     * If factors were already calculated, the
     * @return Factors of a BigInteger to a MultiSet.
     */
    public MultiSet<BigInteger> getFactors() {
        if (factors != null)
            return factors;
        factors = new TreeMultiSet<>();
        while (!Objects.equals(number, bigONE)) {
            BigInteger integer = factorOne(number);
            number = number.divide(integer);
            factors.add(integer);
        }
        return new TreeMultiSet<>(factors);
    }

    /**
     * Gets the original, non-factorized number
     * @return BigInteger number.
     */
    public BigInteger getNumber() {
        return number;
    }
}
