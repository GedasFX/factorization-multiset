package projMilasius;

import projMilasius.Factorization.FactorisableBigInteger;
import projMilasius.Util.MultiSet;
import projMilasius.Util.MultiSetEntry;
import projMilasius.Util.TreeMultiSet;

import java.math.BigInteger;

public class Main {
    public static void main(String[] args) {
        factorize();
    }

    private static void factorize() {
        // Testavimui galima patikrinti svetainese:
        // https://primes.utm.edu/curios/includes/primetest.php
        // https://www.alpertron.com.ar/ECM.HTM
        factorize("789456123789456123");
        factorize("46512308645132");
        factorize("741852963");
        factorize("2048");
        factorize("30481920");
        factorize("7894561894562034574789465123897456123789");
        factorize("7894561894562034574789465123897456123791");
    }

    private static void factorize(String s) {
        FactorisableBigInteger integer = new FactorisableBigInteger(s);
        System.out.println("Integer " + integer.getBigInteger() + " factorizes into: ");
        MultiSet<BigInteger> set = integer.getFactors();
        for (MultiSetEntry<BigInteger> e : set.entrySet()) {
            System.out.printf("%3sx [ %s ]%n", e.getCount(), e.getElement());
        }
        System.out.println();
    }
}
