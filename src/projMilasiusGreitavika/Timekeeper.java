package projMilasiusGreitavika;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;

/**
 * @author eimutis
 */
public class Timekeeper {

    int[] tyrimoImtis;
    private final BlockingQueue<String> resultsLogger;
    private final Semaphore semaphore;
    private long startTime, finishTime;
    private final long startTimeTot;
    private long finishTimeTot;
    private double sumTime;
    private int tyrimoInd;
    private int kiekioInd;
    private int tyrimuN;
    private final int tyrimuNmax = 30;
    private final int kiekioN;
    double[][] laikai;
    private String tyrimuEilute;
    private final String duomFormatas = "%9.4f ";
    private final String normFormatas = "%9.2f ";
    private final String vardoFormatas = "%9s ";
    private final String kiekioFormatas = "%8d(%2d) ";
    private String antraste = "  kiekis(*k) ";

    public Timekeeper(int[] kiekiai, BlockingQueue resultsLogger, Semaphore semaphore) {
        this.tyrimoImtis = kiekiai;
        this.resultsLogger = resultsLogger;
        this.semaphore = semaphore;
        kiekioN = tyrimoImtis.length;
        laikai = new double[kiekioN][tyrimuNmax];
        startTimeTot = System.nanoTime();
    }

    public void start() throws InterruptedException {
        tyrimoInd = 0;
        if (kiekioInd >= kiekioN) {
            logResult("Duomenu kiekis keiciamas daugiau kartu nei buvo planuota");
            // System.exit(0);
        }
        tyrimuEilute = String.format(kiekioFormatas, tyrimoImtis[kiekioInd],
                tyrimoImtis[kiekioInd] / tyrimoImtis[0]);
        Runtime.getRuntime().gc();
        Runtime.getRuntime().gc();
        Runtime.getRuntime().gc();
        startTime = System.nanoTime();
    }

    public void startAfterPause() {
        Runtime.getRuntime().gc();
        Runtime.getRuntime().gc();
        Runtime.getRuntime().gc();
        startTime = System.nanoTime();
    }

    public void finish(String vardas) throws InterruptedException {
        finishTime = System.nanoTime();
        double t1 = (finishTime - startTime) / 1e9;
        sumTime += t1;
        if (startTime == 0) {
            logResult("Metodas finish panaudotas be start metodo !!!\n");
            //   System.exit(0);
        }
        if (kiekioInd == 0) {
            antraste += String.format(vardoFormatas, vardas);
        }
        if (tyrimoInd >= tyrimuNmax) {
            logResult("Jau atlikta daugiau tyrimu nei numatyta  !!!\n");
            //  System.exit(0);
        }
        laikai[kiekioInd][tyrimoInd++] = t1;
        tyrimuEilute += String.format(duomFormatas, t1);
        Runtime.getRuntime().gc();
        Runtime.getRuntime().gc();
        Runtime.getRuntime().gc();
        startTime = System.nanoTime();
    }

    public void seriesFinish() throws InterruptedException {
        if (kiekioInd == 0) {
            logResult(antraste);
        }
        logResult(tyrimuEilute);
        kiekioInd++;
        tyrimuN = tyrimoInd;
        if (kiekioInd == (kiekioN)) {
            summary();
        }
    }

    private void summary() throws InterruptedException {
        StringBuilder sb = new StringBuilder();
        finishTimeTot = System.nanoTime();
        double totTime = (finishTimeTot - startTimeTot) / 1e9;
        sb.append(String.format("       Bendras tyrimo laikas %8.3f sekundžiu", totTime)).append("\n");
        sb.append(String.format("    Ismatuotas tyrimo laikas %8.3f sekundžiu", sumTime)).append("\n");
        sb.append(String.format("    t.y. %5.1f%% sudaro pagalbiniai darbai",
                (totTime - sumTime) / totTime * 100)).append("\n");
        sb.append("\n");
        sb.append("Normalizuota (santykine) laiku lentele\n");
        sb.append(antraste).append("\n");
        double d1 = laikai[0][0];
        for (int i = 0; i < kiekioN; i++) {
            tyrimuEilute = String.format(kiekioFormatas, tyrimoImtis[i],
                    tyrimoImtis[i] / tyrimoImtis[0]);
            for (int j = 0; j < tyrimuN; j++) {
                tyrimuEilute += String.format(normFormatas, laikai[i][j] / d1);
            }
            sb.append(tyrimuEilute).append("\n");
        }
        logResult(sb.toString());
    }

    public void logResult(String result) throws InterruptedException {
        resultsLogger.put(result);
        semaphore.acquire();
    }
}