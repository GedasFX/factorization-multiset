package projMilasiusGreitavika;

public class Greitaveika {
    public static void main(String[] args) {
        GreitaveikosTyrimas gt = new GreitaveikosTyrimas();
        new Thread(() -> {
            try {
                String result;
                while (!(result = gt.getResultsLogger().take())
                        .equals(GreitaveikosTyrimas.FINISH_COMMAND)) {
                    System.out.println(result);
                    gt.getSemaphore().release();
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }

            gt.getSemaphore().release();
        }, "Greitaveikos_rezultatu_gija").start();

        new Thread(gt::pradetiTyrima, "Greitaveikos_tyrimo_gija").start();
    }
}
