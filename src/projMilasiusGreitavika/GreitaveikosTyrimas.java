package projMilasiusGreitavika;

import projMilasius.Util.MultiSet;
import projMilasius.Util.TreeMultiSet;

import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.SynchronousQueue;

public class GreitaveikosTyrimas {

    public static final String FINISH_COMMAND = "finish";

    private static final String[] TYRIMU_VARDAI = { "addBePasi", "addSuPasi", "addVienod", "conBePasi", "conSuPasi", "conVienod", "delBePasi", "delSuPasi", "delVienod" };
    private static final int[] TIRIAMI_KIEKIAI = { 10000, 20000, 40000, 80000, 160000, 320000, 640000 };

    private final BlockingQueue resultsLogger = new SynchronousQueue();
    private final Semaphore semaphore = new Semaphore(-1);
    private final Timekeeper tk;
    private final String[] errors;

    private final MultiSet<Integer> bePasikartojimu = new TreeMultiSet<>();
    private final MultiSet<Integer> suPasikartojimais = new TreeMultiSet<>();
    private final MultiSet<Integer> vienodasElementas = new TreeMultiSet<>();

    public GreitaveikosTyrimas() {
        semaphore.release();
        tk = new Timekeeper(TIRIAMI_KIEKIAI, resultsLogger, semaphore);
        errors = new String[]{
                "Netinkamas generuojamos aibes dydis",
                "Netinkama pradine aibes imtis arba netinkamai nuskaityti duomenys",
                "Generuojama aibe turi bubti didesne negu pradine aibes imtis",
                "Netinkamas isbarstymo koeficientas"
        };
    }

    public void pradetiTyrima() {
        try {
            SisteminisTyrimas();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }

    public void SisteminisTyrimas() throws InterruptedException {
        try {
            for (int k : TIRIAMI_KIEKIAI) {
                bePasikartojimu.clear();
                suPasikartojimais.clear();
                vienodasElementas.clear();

                Integer[] masBePas = generuotiBePasikartojimu(k);
                Integer[] masSuPas = generuotiSuPasikartojimu(k);
                Integer[] masViena = generuotiViena(k);

                tk.startAfterPause();
                tk.start();
                bePasikartojimu.addAll(Arrays.asList(masBePas));
                tk.finish(TYRIMU_VARDAI[0]);
                suPasikartojimais.addAll(Arrays.asList(masSuPas));
                tk.finish(TYRIMU_VARDAI[1]);
                vienodasElementas.addAll(Arrays.asList(masViena));
                tk.finish(TYRIMU_VARDAI[2]);
                for (Integer a : masBePas) {
                    bePasikartojimu.contains(a);
                }
                tk.finish(TYRIMU_VARDAI[3]);
                for (Integer a : masSuPas) {
                    suPasikartojimais.contains(a);
                }
                tk.finish(TYRIMU_VARDAI[4]);
                for (Integer a : masViena) {
                    vienodasElementas.contains(a);
                }
                tk.finish(TYRIMU_VARDAI[5]);
                for (Integer a : masBePas) {
                    bePasikartojimu.remove(a);
                }
                tk.finish(TYRIMU_VARDAI[6]);
                for (Integer a : masSuPas) {
                    suPasikartojimais.remove(a);
                }
                tk.finish(TYRIMU_VARDAI[7]);
                for (Integer a : masViena) {
                    vienodasElementas.remove(a);
                }
                tk.finish(TYRIMU_VARDAI[8]);
                tk.seriesFinish();
            }
            tk.logResult(FINISH_COMMAND);
        } catch (MyException e) {
            if (e.getCode() >= 0 && e.getCode() <= 3) {
                tk.logResult(errors[e.getCode()] + ": " + e.getMessage());
            } else if (e.getCode() == 4) {
                tk.logResult("Visa sugeneruota aibe jau isspausdinta");
            } else {
                tk.logResult(e.getMessage());
            }
        }
    }

    private Integer[] generuotiBePasikartojimu(int amount) {
        Integer[] integers = new Integer[amount];
        int j = 0;
        for (int i = amount/2; i < amount; i++) {
            integers[j++] = i;
            integers[j++] = amount-i;
        }
        return integers;
    }

    private Integer[] generuotiSuPasikartojimu(int amount) {
        int sq = (int) Math.sqrt(amount);
        Integer[] integers = new Integer[amount];
        for (int i = 0; i < amount; i++) {
            integers[i] = i % sq;
        }
        return integers;
    }

    private Integer[] generuotiViena(int amount) {
        Integer[] integers = new Integer[amount];
        Arrays.fill(integers,1);
        return integers;
    }

    public BlockingQueue<String> getResultsLogger() {
        return resultsLogger;
    }

    public Semaphore getSemaphore() {
        return semaphore;
    }

    public class MyException extends RuntimeException {

        // Situacijos kodas. Pagal ji programuojama programos reakcija į situaciją
        private int code;

        public MyException(String text) {
            // (-1) - susitariama, kad tai neutralus kodas.
            this(text, -1);
        }

        public MyException(String message, int code) {
            super(message);
            if (code < -1) {
                throw new IllegalArgumentException("Illegal code in MyException: " + code);
            }
            this.code = code;
        }

        public MyException(String message, Throwable throwable, int code) {
            super(message, throwable);
            if (code < -1) {
                throw new IllegalArgumentException("Illegal code in MyException: " + code);
            }
            this.code = code;
        }

        public int getCode() {
            return code;
        }
    }
}
